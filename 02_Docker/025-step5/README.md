## Env reader

```bash
go run main.go 
```

```bash
docker build . -t env-reader:0.0.1
docker run -it env-reader:0.0.1    
docker run -it -e DB_NAME="stolon" env-reader:0.0.1    
docker tag env-reader:0.0.1 kixualx/env-reader
docker tag env-reader:0.0.1 kixualx/env-reader:0.0.1
docker push kixualx/env-reader:0.0.1
```