## Commands

```bash
docker build . -t go-big:0.0.1
docker build . -t go-tiny:0.0.1
docker images 
```

```Dockerfile
FROM golang:1.20 AS build

WORKDIR /app 

COPY . .

RUN go build -o app .

FROM alpine:latest    
                   
WORKDIR /app/

COPY --from=build ./app .             

CMD ["./app"]
```