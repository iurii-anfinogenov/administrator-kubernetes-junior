# ReplicaSet

```bash
kubectl -n home-dev get pod
kubectl -n home-dev get all
kubectl -n home-dev delete pod http-server
kubectl -n home-dev get pod
kubectl apply -f replicaSet.yaml 
kubectl -n home-dev get replicaset
kubectl -n home-dev get rs
kubectl -n home-dev get po
kubectl -n home-dev get po -w
```